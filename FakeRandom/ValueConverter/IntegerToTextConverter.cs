﻿using System;
using System.Windows.Data;

namespace FakeRandom.ValueConverter
{
    [ValueConversion(typeof(int), typeof(string))]
    public class IntegerToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return string.Format("{0:00}", (int)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return int.Parse((string)value);
        }
    }
}
