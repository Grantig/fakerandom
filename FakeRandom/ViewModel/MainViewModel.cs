﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FakeRandom.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Private Fields

        Random rnd;
        int index;
        List<Tuple<char, int>> results;

        #endregion

        #region Constructor

        public MainViewModel()
        {
            this.index = 0;
            this.RandomLetter = '0';
            this.RandomNumber = 0;
            this.rnd = new Random();

            string[] args = Environment.GetCommandLineArgs();

            if (args.Length > 1 && File.Exists(args[1]))
            {
                this.results = new List<Tuple<char, int>>();

                foreach (string s in File.ReadAllText(args[1]).Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    var trimmed = s.Trim();
                    var tuple = new Tuple<char, int>(trimmed[0], int.Parse(trimmed.Substring(1)));
                    this.results.Add(tuple);
                }
            }
        }

        #endregion

        #region Properties

        private int randomNumber;

        public int RandomNumber
        {
            get { return randomNumber; }
            set { Set(ref randomNumber, value); }
        }


        private char randomLetter;

        public char RandomLetter
        {
            get { return randomLetter; }
            set { Set(ref randomLetter, value); }
        }        

        private RelayCommand rollDiceCommand;

        public RelayCommand RollDiceCommand
        {
            get
            {
                if (this.rollDiceCommand == null)
                {
                    Set(ref this.rollDiceCommand, new RelayCommand(RollDice));
                }

                return this.rollDiceCommand;
            }
        }

        #endregion

        #region Private Methods

        private async void RollDice()
        {
            for (int i = 0; i < 150; i++)
            {
                RandomLetter = (char)rnd.Next(65, 91);
                RandomNumber = rnd.Next(1, 100);
                await Task.Delay(5);
            }

            if (this.results != null && this.index < this.results.Count)
            {
                RandomLetter = this.results[this.index].Item1;
                RandomNumber = this.results[this.index].Item2;
                this.index++;
            }
        }

        #endregion
    }
}
